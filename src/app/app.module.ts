import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { TimeAgoPipe } from 'time-ago-pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollPanelModule } from 'primeng/scrollpanel';

import { appRoutes } from './routes';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { AuthService } from './_services/auth.service';
import { AlertService } from './_services/alert.service';
import { RegisterComponent } from './register/register.component';
import { ChatService } from './_services/chat.service';
import { AuthGuard } from './_guards/auth.guard';
import { PlayerCardComponent } from './players/player-card/player-card.component';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { UserService } from './_services/user.service';
import { PlayerListResolver } from './_resolvers/players-list.resolver';
import { TictactoeComponent } from './play/games/tictactoe/tictactoe.component';
import { GameService } from './_services/game.service';
import { PlayComponent } from './play/play.component';
import { AlertModule } from 'ngx-bootstrap/alert';

export function tokenGetter() {
   return localStorage.getItem('token');
}

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      TimeAgoPipe,
      ChatComponent,
      RegisterComponent,
      ChatComponent,
      PlayerCardComponent,
      PlayerListComponent,
      TictactoeComponent,
      PlayComponent
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      HttpClientModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes),
      BsDropdownModule.forRoot(),
      FormsModule,
      ScrollPanelModule,
      JwtModule.forRoot({
         config: {
            tokenGetter: tokenGetter,
            whitelistedDomains: ['localhost:5000'],
            blacklistedRoutes: ['localhost:5000/api/auth']
         }
      }),
      AlertModule.forRoot()
   ],
   providers: [
      AuthService,
      AlertService,
      ChatService,
      UserService,
      GameService,
      AuthGuard,
      PlayerListResolver

   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
