import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { PlayerListResolver } from './_resolvers/players-list.resolver';
import { TictactoeComponent } from './play/games/tictactoe/tictactoe.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'players',
        component: PlayerListComponent,
        resolve: { users: PlayerListResolver }
      },
      {
        path: 'play',
        children: [
          {
            path: 'tictactoe',
            component: TictactoeComponent
          }
        ]
      }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];
