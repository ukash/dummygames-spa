import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChatMessage } from '../_modules/chatMessage';
import { ChatService } from '../_services/chat.service';
import { AlertService } from '../_services/alert.service';
import { AuthService } from '../_services/auth.service';
import { OnlineUsers } from '../_modules/onlineUsers';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  onlineUsers: OnlineUsers;
  messages: ChatMessage[];
  messageContent: string;
  currentUserId: number;

  constructor(private chatService: ChatService, private alertService: AlertService,
    public authService: AuthService) {
    this.messages = new Array();
    this.onlineUsers = { usersIds: new Array<number>(), guests: 0, total: 0 };
  }

  ngOnInit() {
    this.chatService.init().then(() => {
      // this.alertService.success('Successfully connected to the chat server.');
      this.chatService.receiveMessage
        .subscribe(message => this.receiveMessage(message));
      this.chatService.onlineUsers.subscribe(onlineUsers => {
        this.onlineUsers = onlineUsers;
      });
      this.chatService.receiveMessages
        .subscribe(messages => this.messages = messages);
      this.chatService.getOnlineUsers();
      this.chatService.getMessages();
    })
      .catch(err => this.alertService.error(err));
  }

  sendMessage() {
    this.chatService.sendMessage(this.messageContent);
    this.messageContent = '';
  }
  receiveMessage(message) {
    console.log(message);
    this.messages.push(message);
    this.chatRef.scrollIntoView();
  }
  getOnlineUsers(service) {
    service.receiveOnlineUsers();
  }

  isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }
}
