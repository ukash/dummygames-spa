import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/_modules/User';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';
import { AlertService } from 'src/app/_services/alert.service';
import { GameService } from 'src/app/_services/game.service';
import { GameType } from 'src/app/_modules/gameRequest';

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.css']
})
export class PlayerCardComponent implements OnInit {

  @Input() user: User;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private alert: AlertService,
    private gameService: GameService
  ) { }

  ngOnInit() {
  }


  challengeTest() {
    this.gameService.challenge(this.user.id, 100, GameType.ticTacToe);
  }
}
