import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { AlertService } from 'src/app/_services/alert.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_modules/User';
import { UserParams } from 'src/app/_modules/userParams';
import { PaginationParams } from 'src/app/_modules/paginationParams';
import { GameService } from 'src/app/_services/game.service';
import { GameType } from 'src/app/_modules/gameRequest';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {
  users: User[];
  user: User = JSON.parse(localStorage.getItem('user'));
  userParams: UserParams;
  paginationParams: PaginationParams;

  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.users = data['users'].result;
      this.paginationParams = {
        pageNumber: data['users'].pagination.currentPage,
        pageSize: data['users'].pagination.itemsPerPage
      };
    });
  }

  loadUsers() {
    this.userService.getUsers(this.paginationParams, this.userParams)
      .subscribe(
        res => {
          this.users = res.result;
          this.paginationParams = {
            pageNumber: res.pagination.currentPage,
            pageSize: res.pagination.itemsPerPage
          };
        },
        error => this.alertService.error(error)
      );
  }
}

