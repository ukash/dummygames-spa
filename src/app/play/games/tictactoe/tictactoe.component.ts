import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/_services/game.service';
import { GameState, GameStatus } from 'src/app/_modules/gameState';
import { debug } from 'util';
import { User } from 'src/app/_modules/User';

@Component({
  selector: 'app-tictactoe',
  templateUrl: './tictactoe.component.html',
  styleUrls: ['./tictactoe.component.css']
})
export class TictactoeComponent implements OnInit {
  gameState: GameState;
  currentPlayer: User;

  constructor(private gameService: GameService) {
    // this.board = new Array(3).fill(null).map(() => new Array(3).fill(null));
  }

  ngOnInit() {
    this.gameService.receiveGameState.subscribe(gameState => {
      console.log(gameState);
      this.gameState = gameState;
    });
  }

  updateGame(gs: GameState) {
    console.log(gs);
    this.gameState = gs;
    console.log(this.gameState);
  }

  clickPosition(x: number, y: number) {
    this.gameService.sendGameMove(this.gameState.id, { x, y });
  }

  getCurrentPlayer(): User {
    if (GameStatus !== null) {
      return this.gameState.players.find(u => u.id === this.gameState.currentPlayerId);
    }
    return null;
  }

}
