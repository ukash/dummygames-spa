import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { GameService } from '../_services/game.service';
import { GameRequest, GameRequestStatus } from '../_modules/gameRequest';
import { AlertService } from '../_services/alert.service';
import { GameState, GameStatus, GameType } from '../_modules/gameState';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit {
  route: string;
  isGameInProgress = false;

  constructor(private authService: AuthService, private gameService: GameService,
    private alertService: AlertService, private router: Router) {
    this.router.events
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.route = event.url;
        }
      });
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.gameService.init().then(() => {
        this.gameService.receiveChallenge.subscribe(this.handleGameRequest.bind(this));
        this.gameService.receiveGameState.subscribe(this.handleGameState.bind(this));
      });
    }
  }

  isPlayComponent() {
    return this.route.startsWith('/play/');
  }

  handleGameState(gameState: GameState) {
    this.isGameInProgress = (gameState !== null && gameState.status === GameStatus.inProgress);
  }

  handleGameRequest(gameRequest: GameRequest) {
    const userId = this.authService.currentUser.id;

    switch (gameRequest.status) {
      case GameRequestStatus.inProgress:
        if (userId === gameRequest.senderId) {
          this.alertService.challengeAwait(gameRequest.id, gameRequest.recipientUserName,
            gameRequest.recipientAvatarUrl, gameRequest.gameType, gameRequest.bid)
            .then(hasCancelled => {
              if (hasCancelled) { this.gameService.respondToChallenge(gameRequest.id, GameRequestStatus.cancelled); }
            });
        } else if (userId === gameRequest.recipientId) {
          this.alertService.challengeResponse(gameRequest.id, gameRequest.senderUserName,
            gameRequest.senderAvatarUrl, gameRequest.gameType, gameRequest.bid)
            .then(hasAccepted => {
              if (hasAccepted) {
                this.gameService.respondToChallenge(gameRequest.id, GameRequestStatus.accepted);
              } else {
                this.gameService.respondToChallenge(gameRequest.id, GameRequestStatus.rejected);
              }
            });
        }
        break;
      case GameRequestStatus.rejected:
        if (userId === gameRequest.senderId) {
          this.alertService.hideGameRequest(gameRequest.id);
        }
        break;
      case GameRequestStatus.accepted:
      this.newGameStarted(GameType[gameRequest.gameType]);
        break;

      case GameRequestStatus.timedOut:
      case GameRequestStatus.cancelled:
      case GameRequestStatus.closed:
        this.alertService.hideGameRequest(gameRequest.id);
        break;
    }
  }

  newGameStarted(gameType: string) {
    this.alertService.hideAll();
    this.router.navigate([`/play/${gameType.toLowerCase()}`]);
  }
}
