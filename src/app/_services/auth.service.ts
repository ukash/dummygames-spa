import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../_modules/User';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ChatService } from './chat.service';
import { GameService } from './game.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.apiUrl + 'auth/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  currentUser: User;

  constructor(private http: HttpClient, private chatService: ChatService,
    private gameService: GameService) { }

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model)
      .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('token', user.token);
            localStorage.setItem('user', JSON.stringify(user.user));
            this.decodedToken = this.jwtHelper.decodeToken(user.token);
            this.currentUser = user.user;

            this.gameService.init().then(() => this.chatService.resetConnection());
          }
        })
      );
  }

  register(user: User) {
    return this.http.post(this.baseUrl + 'register', user);
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  logout(): Promise<void> {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    return this.gameService.stopConnection().then(() => this.chatService.resetConnection());
  }

  roleMatch(allowedRoles): boolean {

    const userRoles = this.decodedToken.role as Array<string>;

    for (let i = 0; i < userRoles.length; i++) {
      if (allowedRoles.includes(userRoles[i])) { return true; }
    }

    return false;
  }

}
