import { Injectable } from '@angular/core';
import { GameType } from '../_modules/gameRequest';
import { IziToast, IziToastSettings } from 'izitoast/types/index';

declare let iziToast: IziToast;

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() {
    iziToast.settings({
      position: 'bottomCenter',
      timeout: 3000,
    });
  }

  success(message: string, title?: string, icon?: string) {
    iziToast.success(this.generateModel(message, title, icon));
  }

  error(message: string, title?: string, icon?: string) {
    iziToast.error(this.generateModel(message, title, icon));
  }

  warning(message: string, title?: string, icon?: string) {
    iziToast.warning(this.generateModel(message, title, icon));
  }

  info(message: string, title?: string, icon?: string) {

    iziToast.info(this.generateModel(message, title, icon));
  }

  challengeAwait(requestId: number, recipientName: string, recipientAvatar: string, gameType: GameType, bid: number) {
    return new Promise(resolve =>
      iziToast.show({
        class: `request${requestId}`,
        theme: 'dark',
        image: recipientAvatar || '../../assets/avatar.png',
        title: `${GameType[gameType]} (${bid} points)`,
        message: `You have challenged ${recipientName} to play ${GameType[gameType]} for ${bid} points.`,
        position: 'topLeft',
        layout: 2,
        maxWidth: 350,
        timeout: 30000,
        color: 'white',
        animateInside: true,
        pauseOnHover: false,
        progressBarColor: '#DF691A',
        backgroundColor: '#4e5d6c',
        buttons: [
          ['<button>Cancel</button>', function (instance, toast) {
            instance.hide({
              transitionOut: 'fadeOutLeft',
              onClosing: (_instance, _toast, _closedBy) => {
                return resolve(true);
              }
            }, toast, 'buttonName');
          }, true], // true to focus
        ],
        onClosing: function (_instance, _toast, closedBy) {
          if (closedBy !== undefined) {
            return resolve(true);
          }
          return resolve(false);
        }
      }));
  }

  challengeResponse(requestId: number, senderName: string, senderAvatar: string, gameType: GameType, bid: number) {
    return new Promise(resolve =>
      iziToast.show({
        class: `request${requestId}`,
        theme: 'dark',
        image: senderAvatar || '../../assets/avatar.png',
        title: `${GameType[gameType]} (${bid} points)`,
        message: `${senderName} challenges you to play ${GameType[gameType]} for ${bid} points.`,
        position: 'topLeft',
        layout: 2,
        maxWidth: 350,
        timeout: 30000,
        color: 'white',
        animateInside: true,
        pauseOnHover: false,
        progressBarColor: '#DF691A',
        backgroundColor: '#4e5d6c',
        buttons: [
          ['<button>Accept</button>', function (instance, toast) {
            instance.hide({
              transitionOut: 'flipOutX',
              onClosing: (_instance, _toast, _closedBy) => {
                return resolve(true);
              }
            }, toast, 'buttonName');
          }, true], // true to focus
          ['<button>Reject</button>', function (instance, toast) {
            instance.hide({
              transitionOut: 'fadeOutLeft',
              onClosing: (_instance, _toast, _closedBy) => {
                return resolve(false);
              }
            }, toast, 'buttonName');
          }, false]
        ],
        onClosing: function (_instance, _toast, closedBy) {
          if (closedBy !== undefined) {
            return resolve(false);
          }
        }
      }));
  }

  hideGameRequest(id: number) {
    iziToast.hide({
      transitionOut: 'fadeOutLeft'
    }, `.request${id}`);
  }

  hideAll() {
    iziToast.destroy();
  }

  private generateModel(message: string, title?: string, icon?: string): IziToastSettings {
    const data: IziToastSettings = {};

    data.message = message;
    if (title) { data.title = title; }
    if (icon) { data.icon = icon; }
    return data;
  }
}
