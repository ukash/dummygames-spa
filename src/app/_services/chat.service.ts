import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { fromEvent, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ChatMessage } from '../_modules/chatMessage';
import { OnlineUsers } from '../_modules/onlineUsers';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  connectionUrl = environment.hubsUrl + 'chat';

  private connection: signalR.HubConnection;
  receiveMessage: Subject<ChatMessage>;
  onlineUsers: Subject<OnlineUsers>;
  receiveMessages: Subject<ChatMessage[]>;

  constructor() {
    this.receiveMessage = new Subject<ChatMessage>();
    this.onlineUsers = new Subject<OnlineUsers>();
    this.receiveMessages = new Subject<ChatMessage[]>();
  }

  init() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl(this.connectionUrl, {
        accessTokenFactory: () => localStorage.getItem('token')
      })
      .build();

    fromEvent(this.connection, 'ReceiveMessage').subscribe(this.receiveMessage);
    fromEvent(this.connection, 'ReceiveMessages').subscribe(this.receiveMessages);
    fromEvent(this.connection, 'ReceiveOnlineUsers').subscribe(this.onlineUsers);

    return this.connection.start();
  }

  resetConnection(): Promise<void> {
    return this.connection.stop().then(() => this.init());
  }

  sendMessage(message: string) {
    this.connection.send('SendMessage', message);
  }

  getOnlineUsers() {
    this.connection.send('GetOnlineUsers');
  }
  getMessages() {
    this.connection.send('GetMessages');
  }
}
