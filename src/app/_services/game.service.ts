import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { GameRequest, GameType, GameRequestStatus } from '../_modules/gameRequest';
import { AlertService } from './alert.service';
import { fromEvent, Observable, BehaviorSubject, Subject } from 'rxjs';
import { GameState } from '../_modules/gameState';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  connectionUrl = environment.hubsUrl + 'game';

  private connection: signalR.HubConnection;
  receiveChallenge: Subject<GameRequest>;
  receiveGameState: BehaviorSubject<GameState>;

  constructor(private alertService: AlertService) {
    this.receiveGameState = new BehaviorSubject(null);
    this.receiveChallenge = new Subject();
  }

  init() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl(this.connectionUrl, {
        accessTokenFactory: () => localStorage.getItem('token')
      })
      .build();
    fromEvent(this.connection, 'ReceiveChallenge').subscribe(this.receiveChallenge);
    // fromEvent(this.connection, 'ReceiveGameState').subscribe(gameState => this.receiveGameState.next(gameState as GameState));
    fromEvent(this.connection, 'ReceiveGameState').subscribe(this.receiveGameState);

    return this.connection.start();
  }

  challenge(userId: number, bid: number, gameType: GameType) {
    this.connection.invoke('Challenge', userId, bid, gameType)
      .catch(err => {

        this.alertService.error(this.getHubErrorMessage(err));
      });
  }

  stopConnection(): Promise<void> {
    return this.connection.stop();
  }

  respondToChallenge(id: number, responseStatus: GameRequestStatus) {
    this.connection.invoke('UpdateGameRequest', id, responseStatus)
      .catch(err => {
        this.alertService.error(this.getHubErrorMessage(err));
      });
  }

  sendGameMove(id: number, move) {
    this.connection.invoke('SendGameMove', id, move)
      .catch(err => {
        this.alertService.error(this.getHubErrorMessage(err));
      });
  }

  private getHubErrorMessage(error) {
    if (!error.hasOwnProperty('message') || typeof error.message !== 'string') {
      return '';
    }

    return error.message.indexOf(':') !== -1 ? error.message.split(':')[1].trim() : '';
  }
}
