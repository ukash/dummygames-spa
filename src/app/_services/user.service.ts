import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../_modules/pagination';
import { User } from '../_modules/User';
import { PaginationParams } from '../_modules/paginationParams';
import { UserParams } from '../_modules/userParams';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getUsers(paginationParams?: PaginationParams, userParams?: UserParams): Observable<PaginatedResult<User[]>> {
    const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();

    let params = new HttpParams();

    for (const key in paginationParams) {
      if (paginationParams.hasOwnProperty(key) && paginatedResult[key] !== null) {
        params = params.append(key, paginationParams[key]);
      }
    }

    for (const key in userParams) {
      if (userParams.hasOwnProperty(key) && userParams[key] !== null) {
        params = params.append(key, userParams[key]);
      }
    }

    return this.http.get<User[]>(this.baseUrl + 'users', { observe: 'response', params })
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') !== null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }

  // getUser(id): Observable<User> {
  //   return this.http.get<User>(this.baseUrl + 'users/' + id);
  // }
}
