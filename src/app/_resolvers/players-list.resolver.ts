import { Injectable } from '@angular/core';
import { User } from '../_modules/user';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PaginationParams } from '../_modules/paginationParams';

@Injectable()
export class PlayerListResolver implements Resolve<User[]> {
  paginationParams: PaginationParams = {
    pageNumber: 1,
    pageSize: 5
  };

  constructor(
    private userService: UserService,
    private router: Router,
    private alertService: AlertService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<User[]> {
    return this.userService.getUsers(this.paginationParams).pipe(
      catchError(error => {
        this.alertService.error('Problem retrieving data.');
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }
}
