import { User } from './User';

export interface GameState {
id: number;
players: User[];
status: GameStatus;
gameType: GameType;
pot: number;
winnerId: number | null;
state: any;
currentPlayerId: number;
turnEndAt: Date | string;
}



export enum GameType {
ticTacToe
}

export enum GameStatus {
inProgress,
finished,
cancelled
}
