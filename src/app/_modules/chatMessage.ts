export interface ChatMessage {
 id: Number;
 senderUserName: string;
 senderAvatarUrl: string;
 dateSent: Date;
 content: string;
}

