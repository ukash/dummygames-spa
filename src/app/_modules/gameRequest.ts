export interface GameRequest {
id: number;
senderId: number;
senderUserName: string;
senderAvatarUrl: string;
recipientId: number;
recipientUserName: string;
recipientAvatarUrl: string;
dateSent: Date;
bid: number;
gameType: GameType;
status: GameRequestStatus;
}

export enum GameRequestStatus {
inProgress,
rejected,
accepted,
timedOut,
cancelled,
closed
}
export enum GameType {
ticTacToe
}
