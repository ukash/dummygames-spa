export interface User {
  id: number;
  userName: string;
  email: string;
  avatarUrl: string;
  balance: number;
  created: Date;
  lastActive: Date;
  roles?: String[];
}
