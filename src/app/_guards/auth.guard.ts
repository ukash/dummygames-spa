import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService
  ) { }

  canActivate(next: ActivatedRouteSnapshot): boolean {
    const roles = next.firstChild.data['roles'] as Array<string>;

    if (roles) {
      if (this.authService.roleMatch(roles)) {
        return true;
      } else {
        this.router.navigate(['dashboard']);
        this.alertService.error('You are not authorized to access this area.');
      }
    }

    if (this.authService.isLoggedIn()) {
      return true;
    }

    this.alertService.error('You are not logged in.');
    this.router.navigate(['/home']);
  }
}
